# ticker-client
The ticker-client assumes that the [ticker-api](https://gitlab.com/tobinwalton/ticker-api) is running locally at port 8000.
Data is loaded automatically on startup.

A working npm installation is required.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
